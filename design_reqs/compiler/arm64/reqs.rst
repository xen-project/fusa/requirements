Assumption of Use on the compiler
=================================

Provide GCC version supported by Xen
------------------------------------

`XenSSR~arm64_gcc_version~1`

Description:
GCC version shall be 5.1 or later.

Rationale:

Covers:
 -

Needs:
 - XenValTestCase
