Domain partially emulated resources requirements
================================================

This section lists the requirements related to resources accessible
from the domain that are partially emulated.

Examples:
    | 1. Interrupt Controller
    | 2. Timer
    | 3. PCI Root Complex
