Dom0less Domain creation requirements
=====================================

The following are the requirements related to dom0less domain creation.

Create dom0less domain with specified kernel image (AArch64 linux image)
------------------------------------------------------------------------

`XenSSR~arm64_create_domain_kernel_linux_image~1`

Description:
Xen shall create a domain using a specified kernel image.

Rationale:

Covers:
 - `XenPRQ~static_vms_configuration~1`

Needs:
 - XenValTestCase

Create dom0less domain with specified kernel image (AArch64 linux image - Gzip compressed)
------------------------------------------------------------------------------------------

`XenSSR~arm64_create_domain_kernel_gzip_image~1`

Description:
Xen shall create a domain using a specified kernel image.

Rationale:

Covers:
 - `XenPRQ~static_vms_configuration~1`

Needs:
 - XenValTestCase
