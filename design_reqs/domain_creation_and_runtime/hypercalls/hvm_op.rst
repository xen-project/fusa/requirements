Hypercall hvm_op
================

The following are the requirements related to __HYPERVISOR_hvm_op
hypercall exposed by Xen to Arm64 and AMD64 PVH domains.

Requirements for both Arm64 and AMD64 PVH
=========================================

Map Xenstore shared page using the value returned by the HVMOP_get_param command
--------------------------------------------------------------------------------

`XenSSR~map_xenstore_shared_page_using_hyp_hvm_op~1`

Description:
Domain shall make use of the value returned by hypercall
__HYPERVISOR_hvm_op for inter-domain Xenstore communications.

Rationale:

Covers:
 - `XenPRQ~hvm_hypercall~1`

Needs:
 - XenValTestCase

Bind Xenstore event channel using the value returned by the HVMOP_get_param command
-----------------------------------------------------------------------------------

`XenSSR~bind_xenstore_event_channel_using_hyp_hvm_op~1`

Description:
Domain shall make use of the value returned by hypercall
__HYPERVISOR_hvm_op for inter-domain Xenstore communications.

Rationale:

Covers:
 - `XenPRQ~hvm_hypercall~1`

Needs:
 - XenValTestCase
