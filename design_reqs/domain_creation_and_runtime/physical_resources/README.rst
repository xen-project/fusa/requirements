Physical resources
==================

This section lists the requirements related to physical resources directly
accessible from the domain as well as physical resources entirely controlled
by Xen and invisible to a domain. The later group of resources, although being
invisible to a domain, has an impact on it.

Examples of domain physical resources:
    | 1. PCI device
    | 2. Platform device
    | 3. MMU stage 1

Examples of Xen physical resources:
    | 1. IOMMU stage 2
    | 2. MMU stage 2
