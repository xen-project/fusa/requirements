Generic Timer
=============

The following are the requirements related to ARM Generic Timer interface
exposed by Xen to Arm64 domains.

Probe the Generic Timer device tree node from a domain
------------------------------------------------------

`XenSSR~arm64_probe_generic_timer_dt~1`

Description:
Xen shall generate a device tree node for the Generic Timer (in accordance to
ARM architected timer device tree binding [2]) to allow domains to probe it.

Rationale:

Covers:
 - `XenPRQ~emulated_timer~1`

Needs:
 - XenValTestCase

Read system counter frequency
-----------------------------

`XenSSR~arm64_read_system_counter_freq~1`

Description:
Domain shall read the frequency of the system counter (either via
CNTFRQ_EL0 register or "clock-frequency" device tree property if present).

Rationale:

Covers:
 - `XenPRQ~emulated_timer~1`

Needs:
 - XenValTestCase
