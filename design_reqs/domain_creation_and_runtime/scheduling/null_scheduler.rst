Null scheduler
==============

The following are the requirements related to Xen Null scheduler providing
simple static scheduling policy that always schedules the same vCPU(s)
on the same pCPU(s) for Arm64 and AMD64 PVH domains.

Use Null scheduler if selected via Xen command line
---------------------------------------------------

`XenSSR~use_null_sched_if_selected_via_xen_cmd_line~1`

Description:
Xen shall use the Null scheduler for boot time domains if specified by passing
"sched=null" to the Xen command line. Xen shall automatically perform
the vCPU-pCPU assignments.

Rationale:

Covers:
 - `XenPRQ~scheduling~1`

Needs:
 - XenValTestCase

Configure vCPU-pCPU assignments
-------------------------------

`XenSSR~configure_vcpu_pcpu_assignments~1`

Description:
Xen shall allow the configuration of what pCPU(s) to statically assign all
vCPU(s) for the given boot time domain using the boot time cpupool feature.
The purpose of the cpupool is to run own scheduler on a dedicated set
of pCPUs. The vCPU-pCPU assignments on a per-vCPU basis cannot be configured.

Rationale:

Covers:
 - `XenPRQ~scheduling~1`

Needs:
 - XenValTestCase

Statically assign each vCPU to a pCPU at the boot time
------------------------------------------------------

`XenSSR~statically_assign_each_vcpu_to_pcpu_at_boot_time~1`

Description:
Xen shall statically assign each vCPU to a pCPU at the boot time. If there are
less vCPUs than pCPUs, Xen shall keep pCPUs without any vCPU assigned in idle
(run their idle vCPUs). If there are more vCPUs than pCPUs, Xen shall add vCPUs
that are not assigned to any pCPU to the wait queue. Xen should not run these
vCPUs until they get assigned.

Rationale:

Covers:
 - `XenPRQ~scheduling~1`

Needs:
 - XenValTestCase

Run the same vCPU on each pCPU
------------------------------

`XenSSR~run_same_vcpu_on_each_pcpu~1`

Description:
Xen shall always run the same online vCPU on each pCPU or nothing (run its idle
vCPU) if the pCPU is idle. Xen should not move vCPU between pCPUs while
executing hot paths.

Rationale:

Covers:
 - `XenPRQ~scheduling~1`

Needs:
 - XenValTestCase
