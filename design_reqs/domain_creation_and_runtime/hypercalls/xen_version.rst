Hypercall xen_version
=====================

The following are the requirements related to __HYPERVISOR_xen_version hypercall
exposed by Xen to Arm64 and AMD64 PVH domains.

Validate the page size returned by XENVER_pagesize command
----------------------------------------------------------

`XenSSR~validate_page_size_xenver_pagesize_cmd~1`

Description:
For Arm64 and AMD64 PVH, the returned page size should be 4KB.

Rationale:

Covers:
 - `XenPRQ~version_hypercall~1`

Needs:
 - XenValTestCase
