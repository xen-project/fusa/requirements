Domain Creation And Runtime
===========================

Emulated UART
-------------

`XenPRQ~emulated_uart~1`

Description:
Xen shall emulate Arm SBSA UART on behalf of the domains.

Rationale:
The domains can use it to write/read to/from the console.

Covers:
 - `XenMRQ~run_arm64_vms~1`
 - `XenMRQ~non_pv_vms_support~1`
 - `XenMRQ~provide_console_vms~1`

Needs:
 - XenSSR

Emulated Timer
--------------

`XenPRQ~emulated_timer~1`

Description:
Xen shall emulate Arm Generic Timer timer on behalf of domains.

Rationale:
The domains can use it for e.g. scheduling.

Covers:
 - `XenMRQ~run_arm64_vms~1`
 - `XenMRQ~non_pv_vms_support~1`
 - `XenMRQ~provide_timer_vms~1`

Needs:
 - XenSSR

Version Hypercall
-----------------

`XenPRQ~version_hypercall~1`

Description:
Xen shall provide an interface to expose Xen version, type and compile
information.

Rationale:

Covers:
 - `XenMRQ~run_arm64_vms~1`
 - `XenMRQ~run_x86_vms~1`

Needs:
 - XenSSR
