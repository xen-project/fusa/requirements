Security
========

The following are the security related requirements.

Prevent a VM from reading data of another VM
--------------------------------------------

`XenSSR~prevent_vm_read_data_of_another_vm~1`

Description:
Xen shall prevent a VM from accessing unauthorised data of another VM or any
unauthorised memory location.

Rationale:
There have been some instances of this violation recorded and fixed in Xen
project as following.

* https://xenbits.xen.org/xsa/advisory-404.html.
* https://xenbits.xen.org/xsa/advisory-442.html.
* https://xenbits.xen.org/xsa/advisory-445.html.
* https://xenbits.xen.org/xsa/advisory-449.html.

Covers:
 - `XenPRQ~vms_memory_isolation~1`

Needs:
 - XenValTestCase

Prevent VM from crashing Xen
----------------------------

`XenSSR~prevent_vm_crashing_xen~1`

Description:
Xen can intercept certain memory access of VMs. It can emulate instructions for
VMs. However, Xen shall ensure that this does not cause itself to crash
resulting in denial of service of the entire host.

Rationale:
There have been some instances of this violation recorded and fixed in Xen project
https://xenbits.xen.org/xsa/advisory-451.html

Covers:
 - `XenPRQ~vms_denial_of_service~1`

Needs:
 - XenValTestCase
