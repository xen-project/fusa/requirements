System Memory Management Unit, version 3
========================================

The following are the requirements related to Arm System Memory Management
Unit, version 3 interface (hereafter, SMMUv3) entirely controlled
by Xen for performing stage-2 address translations and access protection
functionalities for DMA-capable devices assigned to Arm64 domains.

Probe SMMU device tree node from Xen
------------------------------------

`XenSSR~arm64_probe_smmu_dt_node~1`

Description:
Xen shall probe a device tree node for the SMMU (in accordance to ARM SMMUv3
device tree binding [2]) to retrieve resources (MMIO region, interrupts, etc).

Rationale:

Covers:
 - `XenPRQ~device_passthrough~1`

Needs:
 - XenValTestCase

Skip SMMU device tree node for a domain
---------------------------------------

`XenSSR~arm64_skip_smmu_dt_node_for_domain~1`

Description:
Xen shall skip the SMMU device tree node when creating the device tree for
the hardware domain even if the SMMU cannot be used in Xen. Xen shall also skip
the IOMMU-specific device tree properties of master devices behind that SMMU
such as "iommus", "iommu-map" and "iommu-map-mask".

Rationale:

Covers:
 - `XenPRQ~device_passthrough~1`

Needs:
 - XenValTestCase
