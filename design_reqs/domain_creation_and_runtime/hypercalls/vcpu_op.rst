Hypercall vcpu_op
=================

The following are the requirements related to __HYPERVISOR_vcpu_op hypercall
exposed by Xen to Arm64 and x86_64 domains.

Register vcpu_info structure using VCPUOP_register_vcpu_info command
--------------------------------------------------------------------

`XenSSR~register_vcpu_op_struct~1`

Description:
Domain shall make use of the hypercall vcpu_op passing command
VCPUOP_register_vcpu_info to register vcpu_info structure.

Rationale:

Covers:
 - `XenPRQ~vcpu_op_hypercall~1`

Needs:
 - XenValTestCase

Register vcpu_runstate_info structure using VCPUOP_register_runstate_memory_area command
----------------------------------------------------------------------------------------

`XenSSR~register_vcpu_runstate_info_struct_using_vcpuop~1`

Description:
Domain shall make use of the hypercall vcpu_op passing command
VCPUOP_register_runstate_memory_area to register vcpu_runstate_info structure.

Rationale:

Covers:
 - `XenPRQ~vcpu_op_hypercall~1`

Needs:
 - XenValTestCase
