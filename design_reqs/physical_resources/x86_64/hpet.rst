HPET
====

The following are the requirements related to HPET for Xen booting on AMD64.

Read HPET REV_ID field
----------------------

`XenSSR~x86_64_read_hpet_rev_id~1`

Description:
REV_ID field of General Capabilities and ID Register shall not be 0.

Rationale:

Covers:
 - `XenPRQ~hypervisor_bootloader~1`

Needs:
 - XenValTestCase

Read HPET period
----------------

`XenSSR~x86_64_read_hpet_period~1`

Description:
HPET period shall be within range 100ps <= period <= 100ns.

Rationale:

Covers:
 - `XenPRQ~hypervisor_bootloader~1`

Needs:
 - XenValTestCase
