PSCI
====

The following are requirements on PSCI, and assumptions on TF-A (or
alternative). Only PSCI versions 0.2 and 1.0 are in scope.

Get PSCI version
----------------

`XenSSR~arm64_get_psci_version~1`

Description:
Xen shall invoke psci (PSCI_VERSION) to obtain the version.

Rationale:

Covers:
 - `XenPRQ~hypervisor_fw_interface~1`

Needs:
 - XenValTestCase

Get PSCI features
-----------------

`XenSSR~arm64_get_psci_features~1`

Description:
Xen shall invoke psci (PSCI_FEATURES) to obtain the features.

Rationale:

Covers:
 - `XenPRQ~hypervisor_fw_interface~1`

Needs:
 - XenValTestCase
