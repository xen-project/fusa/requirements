Booting
=======

The following are the requirements related to Xen booting on AMD64.

Use 4KB page granularity
------------------------

`XenSSR~x86_64_use_4kb_page_size~1`

Description:
Xen shall use 4KB page granularity.

Rationale:

Covers:
 - `XenPRQ~hypervisor_bootloader~1`

Needs:
 - XenValTestCase

Zero BSS
--------

`XenSSR~x86_64_zero_bss~1`

Description:
Xen shall zero BSS region.

Rationale:

Covers:
 - `XenPRQ~hypervisor_bootloader~1`

Needs:
 - XenValTestCase
