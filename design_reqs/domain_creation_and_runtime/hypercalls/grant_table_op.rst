Hypercall grant_table_op
========================

The following are the requirements related to __HYPERVISOR_grant_table_op
hypercall exposed by Xen to Arm64 and AMD64 PVH domains.

Requirements for both Arm64 and AMD64 PVH
=========================================

Map the grant entry using hypercall grant_table_op (CPU access)
---------------------------------------------------------------

`XenSSR~map_grant_table_entry_using_hyp_grant_table_op_cpu~1`

Description:
Domain shall make use of the hypercall __HYPERVISOR_grant_table_op
passing GNTTABOP_map_grant_ref as a command with GNTMAP_host_map as
a flag value to obtain a host CPU read-write access to the page granted by
a foreign domain.

Rationale:

Covers:
 - `XenPRQ~grant_table_hypercall~1`

Needs:
 - XenValTestCase

Map the grant entry using hypercall grant_table_op (RO access)
--------------------------------------------------------------

`XenSSR~map_grant_table_entry_using_hyp_grant_table_op_ro~1`

Description:
Domain shall make use of the hypercall __HYPERVISOR_grant_table_op
passing GNTTABOP_map_grant_ref as a command with GNTMAP_readonly as
a flag value to obtain a CPU read-only access to the page granted by
a foreign domain.

Rationale:

Covers:
 - `XenPRQ~grant_table_hypercall~1`

Needs:
 - XenValTestCase
