Hypercall memory_op
===================

The following are the requirements related to __HYPERVISOR_memory_op
hypercall exposed by Xen to Arm64 and AMD64 PVH domains.

Requirements for both Arm64 and AMD64 PVH
=========================================

Query current memory reservation using hypercall memory_op
----------------------------------------------------------

`XenSSR~query_current_memory_reservation_using_hyp_memory_op~1`

Description:
Domain shall be able to make use of the hypercall __HYPERVISOR_memory_op
passing XENMEM_current_reservation as a command to retrieve the number
of pages currently possessed by a domain.

Rationale:

Covers:
 - `XenPRQ~memory_hypercall~1`

Needs:
 - XenValTestCase

Query maximum memory reservation using hypercall memory_op
----------------------------------------------------------

`XenSSR~query_max_memory_reservation_using_hyp_memory_op~1`

Description:
Domain shall make use of the hypercall __HYPERVISOR_memory_op
passing XENMEM_maximum_reservation as a command to retrieve the maximum number
of pages that can be possessed by a domain.

Rationale:

Covers:
 - `XenPRQ~memory_hypercall~1`

Needs:
 - XenValTestCase
