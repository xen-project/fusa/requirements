Hypercall console_io
====================

The following are the requirements related to __HYPERVISOR_console_io hypercall
exposed by Xen to Arm64 and x86_64 domains.

Transmit data using hypercall __HYPERVISOR_console_io
-----------------------------------------------------

`XenSSR~transmit_data_hyp_console_io~1`

Description:
Domain shall make use of the hypercall __HYPERVISOR_console_io
to transmit data.

Rationale:

Covers:
 - `XenPRQ~console_io_hypercall~1`

Needs:
 - XenValTestCase

Receive data using hypercall __HYPERVISOR_console_io
----------------------------------------------------

`XenSSR~receive_data_hyp_console_io~1`

Description:
Domain shall make use of the hypercall __HYPERVISOR_console_io
to receive data.

Rationale:

Covers:
 - `XenPRQ~console_io_hypercall~1`

Needs:
 - XenValTestCase
