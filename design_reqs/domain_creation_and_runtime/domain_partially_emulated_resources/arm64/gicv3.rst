Generic Interrupt Controller, version 3
=======================================

The following are the requirements related to ARM Generic Interrupt
Controller, version 3 interface (hereafter, GICv3) exposed by Xen
to Arm64 domains.

Probe GIC device tree node from a domain
----------------------------------------

`XenSSR~arm64_probe_gic_dt_node~1`

Description:
Xen shall generate a device tree node for the GIC (in accordance to GICv3
device tree binding) to allow domains to probe it. Xen shall also insert
the Interrupt Translation Service (ITS) device tree sub-node (in case the ITS
is emulated by Xen), it shall be a physical ITS node for the hardware domain
and the virtual ITS node for the domUs.

Rationale:

Covers:
 - `XenPRQ~virtual_interrupt_controller~1`

Needs:
 - XenValTestCase

Send Software Generated Interrupts
----------------------------------

`XenSSR~arm64_send_sgis~1`

Description:
Domain shall trigger Software Generated Interrupts (SGIs)
using the System register interface

Rationale:

Covers:
 - `XenPRQ~virtual_interrupt_controller~1`

Needs:
 - XenValTestCase
