Local Advanced Programmable Interrupt Controller
================================================

The following are the requirements related to Local Advanced Programmable
Interrupt Controller (hereafter, LAPIC) exposed by Xen to AMD64 PVH
domains.

Xen shall expose LAPIC to domain
--------------------------------

`XenSSR~x86_64_xen_shall_expose_lapic_to_domain~1`

Description:
Xen shall emulate LAPIC for the AMD64 PVH domain in xAPIC and x2APIC modes.
Xen shall expose LAPIC information to the domain in the ACPI tables.

Rationale:

Covers:
 - `XenPRQ~virtual_interrupt_controller~1`

Needs:
 - XenValTestCase

Use xAPIC mode from domain
--------------------------

`XenSSR~x86_64_use_apic_mode_from_domain~1`

Description:
Domain shall configure and use LAPIC in legacy xAPIC operational
mode. The MMIO-based interface is used for accessing the APIC register set
(except the APIC Base Address Register). Domain should not access the APIC
register set using the MSR-based interface in xAPIC mode.
The domain shall detect the presence of the APIC module by executing
the CPUID instruction. Support of the APIC module is indicated by CPUID
Fn0000_0001_EDX[APIC] (bit 9) = 1.

Rationale:

Covers:
 - `XenPRQ~virtual_interrupt_controller~1`
