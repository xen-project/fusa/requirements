Domain creation and runtime requirements
========================================

Xen creates different VMs (also known as domains) based on the
provided VM creation configuration parameters, such as the amount of
memory and the number of virtual CPUs. In addition to CPUs and
memory, the VM will have access to a timer, an interrupt controller,
a UART, and optionally other hardware resources. The hardware
resources available to the VM might be fully emulated, partially emulated,
or directly accessible (physical resources) to the VM. There are also
physical resources entirely controlled by Xen that impact VMs while being
invisible to them.

Examples of domain fully emulated resources:
    | 1. UART

Examples of domain partially emulated resources:
    | 1. Interrupt Controller
    | 2. Timer
    | 3. PCI Root Complex

Examples of domain physical (directly accessible) resources:
    | 1. MMU stage 1
    | 2. A PCI device, such as a NIC, directly assigned

Examples of Xen physical resources:
    | 1. IOMMU stage 2
    | 2. MMU stage 2
