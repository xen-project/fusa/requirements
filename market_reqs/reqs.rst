Functional Requirements
=======================

Run Arm64 VMs
-------------

`XenMRQ~run_arm64_vms~1`

Description:
Xen shall run Arm64 VMs.

Rationale:

Needs:
 - XenPRQ

Run AMD-x86 VMs
---------------

`XenMRQ~run_x86_vms~1`

Description:
Xen shall run AMD-x86 VMs.

Rationale:

Needs:
 - XenPRQ

Support non paravirtualised VMs
-------------------------------

`XenMRQ~non_pv_vms_support~1`

Description:
Xen shall support running guests which are not virtualisation aware.

Rationale:

Needs:
 - XenPRQ

Provide console to the VMs
--------------------------

`XenMRQ~provide_console_vms~1`

Description:
Xen shall provide a console to a VM.

Rationale:

Needs:
 - XenPRQ

Provide timer to the VMs
------------------------

`XenMRQ~provide_timer_vms~1`

Description:
Xen shall provide a timer to a VM.

Rationale:

Needs:
 - XenPRQ
