Domain fully emulated resources requirements
============================================

This section lists the requirements related to resources accessible
from the domain that are fully emulated.

Examples:
    | 1. UART
