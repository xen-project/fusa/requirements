Assumption of Use on the compiler
=================================

Provide GCC version supported by Xen
------------------------------------

`XenSSR~x86_64_gcc_version~1`

Description:
GCC version shall be 4.1.2_20070115 or later.

Rationale:

Covers:
 -

Needs:
 - XenValTestCase
