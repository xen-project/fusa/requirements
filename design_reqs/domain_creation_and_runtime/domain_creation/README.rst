Domain creation requirements
============================

This section lists the configuration parameter requirements for creating VMs
(also known as domains) on Arm64 and AMD-x86 platforms.
