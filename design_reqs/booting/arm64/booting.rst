Booting
=======

The following are the requirements related to Xen booting on Arm64.

Enable MMU
----------

`XenSSR~arm64_enable_mmu~1`

Description:
Xen shall enable Memory Management Unit.

Rationale:

Covers:
 - `XenPRQ~hypervisor_bootloader~1`

Needs:
 - XenValTestCase

Enable data cache
-----------------

`XenSSR~arm64_enable_dcache~1`

Description:
Xen shall enable data cache.

Rationale:

Covers:
 - `XenPRQ~hypervisor_bootloader~1`

Needs:
 - XenValTestCase
