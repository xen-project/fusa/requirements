Board Support Package
=====================

This section lists the requirements related to the interface between Xen and the
board support package (which comprises of various firmware blobs like u-boot,
BIOS, Arm Trusted Firmware, device tree blob, etc).
