UART
====

The following are the requirements related to SBSA UART emulated and
exposed by Xen to Arm64 domains.

Probe the UART device tree node
-------------------------------

`XenSSR~arm64_uart_probe_dt_node~1`

Description:
Xen shall generate a device tree node for the SBSA UART (in accordance to Arm
SBSA UART device tree binding [2]) to allow domains to probe it.

Rationale:

Covers:
 - `XenPRQ~emulated_uart~1`

Needs:
 - XenValTestCase

Transmit data in software polling mode
--------------------------------------

`XenSSR~arm64_uart_transmit_data_poll_mode~1`

Description:
Domain shall transmit data in polling mode (i.e. without involving
interrupts).

Rationale:

Covers:
 - `XenPRQ~emulated_uart~1`

Needs:
 - XenValTestCase
