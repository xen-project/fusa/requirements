Device Passthrough
==================

The following are the requirements related to a physical device
assignment to Arm64 and AMD64 PVH domains.

Requirements for both Arm64 and AMD64 PVH
=========================================

Hide IOMMU from a domain
------------------------

`XenSSR~hide_iommu_from_domain~1`

Description:
Xen shall not expose the IOMMU device to the domain even if I/O virtualization
is disabled. The IOMMU should be under hypervisor control only.

Rationale:

Covers:
 - `XenPRQ~device_passthrough~1`

Needs:
 - XenValTestCase

Discover PCI devices from hardware domain
-----------------------------------------

`XenSSR~discover_pci_devices_from_hwdom~1`

Description:
The hardware domain shall enumerate and discover PCI devices and
inform Xen about their appearance and disappearance

Rationale:

Covers:
 - `XenPRQ~device_passthrough~1`

Needs:
 - XenValTestCase

Discover PCI devices from Xen
-----------------------------

`XenSSR~discover_pci_devices_from_xen~1`

Description:
Xen shall be able to discover PCI devices (enumerated by the firmware
beforehand) during boot if the hardware domain is not meant to be used

Rationale:

Covers:
 - `XenPRQ~device_passthrough~1`

Needs:
 - XenValTestCase

Assign PCI device to domain (with IOMMU)
----------------------------------------

`XenSSR~assign_pci_device_with_iommu~1`

Description:
Xen shall be able to assign a specified PCI device (always implied as
DMA-capable) to a domain during its creation using passthrough (partial)
device tree. The physical device to be assigned is protected by the IOMMU.

Rationale:

 - The passthrough device tree is specified using a device tree module node
   with compatible ("multiboot,device-tree") in the host device tree
 - The PCI device to be passed through is specified using device tree property
   ("xen,pci-assigned") in the "passthrough" node described in the passthrough
   device tree

Covers:
 - `XenPRQ~device_passthrough~1`

Needs:
 - XenValTestCase
