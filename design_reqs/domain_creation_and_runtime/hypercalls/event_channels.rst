Hypercall event_channel
=======================

The following are the requirements related to __HYPERVISOR_event_channel_op
hypercall exposed by Xen to Arm64 and x86_64 domains.

Requirements for both Arm64 and x86_64
======================================

Allocate an unbound event channel
---------------------------------

`XenSSR~allocate_unbound_event_channel~1`

Description:
Domain shall make use of __HYPERVISOR_event_channel_op passing
EVTCHNOP_alloc_unbound as a command to allocate an unbound event channel.

Rationale:

Covers:
 - `XenPRQ~event_channel_hypercall~1`

Needs:
 - XenValTestCase

Bind event channel to a remote domain
-------------------------------------

`XenSSR~bind_event_channel_to_remote_domain~1`

Description:
Domain shall make use of __HYPERVISOR_event_channel_op passing
EVTCHNOP_bind_interdomain as a command to bind interdomain event channel.

Rationale:

Covers:
 - `XenPRQ~event_channel_hypercall~1`

Needs:
 - XenValTestCase
